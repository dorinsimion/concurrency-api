package com.sda.concurrency.deadlock;

public class PrintAndScan {
    private Printer printer;
    private Scanner scanner;

    public PrintAndScan(Printer printer, Scanner scanner) {
        this.printer = printer;
        this.scanner = scanner;
    }

    public void printAndScan(){
        System.out.println("before print2");
        synchronized (printer){
            printer.print();
            System.out.println("after print2");
            System.out.println("before scan2");
            synchronized (scanner){
                scanner.scan();
            }
            System.out.println("after scan 2");
        }
    }
}
