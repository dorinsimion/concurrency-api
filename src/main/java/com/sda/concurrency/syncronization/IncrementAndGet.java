package com.sda.concurrency.syncronization;

public class IncrementAndGet implements Runnable {
    private int counter;

    @Override
    synchronized public void run() {
        //durea multe
        System.out.println("before");
        //dureaz putin
        synchronized (this) {
            for (int i = 0; i < 10; i++) {
                System.out.println(++counter);
            }
        }
        System.out.println("after");
        //taskuri care dureaza mult
    }
}
